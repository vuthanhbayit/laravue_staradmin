<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Cache;

class CachePage
{
    public function handle($request, Closure $next)
    {
        if ($request->isMethod('post')) {
            $response = $next($request);
            return $response;
        } else {
            $key = $request->fullUrl() . '_' . app()->getLocale();
            if (Cache::has($key) && getConfig('config_debug') == 0) {
                return response(Cache::get($key));
            }
            $response = $next($request);
            if (getConfig('config_debug') == 0) {
                $expiresAt = Carbon::now()->addHours(24);
                Cache::put($key, $response->getContent(), $expiresAt);
            }
            return $response;
        }
    }
}
