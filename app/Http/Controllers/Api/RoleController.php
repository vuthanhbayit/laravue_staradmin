<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use DB, Auth;
use Entrust;

class RoleController extends Controller
{

    public function index(Role $role)
    {
        $role = $role->getDataRole();
        $arrData = array();
        foreach($role as $item) {
            $permission_id = array();
            foreach ( $item['RolePermission'] as $rolePermission){
                $permission_id[] = $rolePermission->permission_id;
            }
            $arrData['role'][] = array(
                'id' => $item->id,
                'name' =>  $item->name,
                'display_name' => $item->display_name,
                'description' =>  $item->description,
                'permission_id' => $permission_id,
            );
        }
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => $arrData
        ]);
    }

    public function createRoles(Request $request)
    {
            $role = new Role();
            $role->name = $request->name;
            $role->display_name = $request->display_name;
            $role->description = $request->description;
            $role->save();
            return response()->json(['success' => 'Create roles success']);
    }

    public function detailRole(Request $request)
    {
        $id = $request->id;
        $role = Role::find($id);
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => $role
        ]);
    }
    public function saveRole(Request $request, Role $role)
    {
        $id = $request->id;
        if($id > 0){
            $role->editRole($request);
            return response()->json([
                'success' => true,
                'message' => 'Update roles success',
            ]);
        }else{
            $role->addRole($request);
            return response()->json([
                'success' => true,
                'message' => 'Create roles success',
            ]);
        }

    }
    public function savePermissionRole(Request $request)
    {
        $rolesId = $request->rolesId;
        $index =  $request->index;
        $role = Role::find($rolesId);
        DB::table('permission_role')->where('role_id', $rolesId)->delete();
        foreach ($request->permission as $key => $value) {
            $role->attachPermission((int)$value);
        }
        return response()->json([
            'success' => true,
            'message' => '',
        ],200);
    }

    public function deleteRole(Request $request)
    {
        $id = $request->id;
        $totalRow = DB::table('role_user')->where('role_id',$id)->get();
        if($totalRow->count() > 0) {
            return response()->json([
                'success' => false,
                'message' => 'You must delete user before delete roles',
            ]);
        }else{
            DB::table('roles')->where('id', $id)->delete();
            DB::table('permission_role')->where('role_id', $id)->delete();
            return response()->json([
                'success' => true,
                'message' => 'Delete Success',
            ]);
        }
    }
    
}
