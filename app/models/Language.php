<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Language extends Model
{
    protected $table = 'language';

    protected $fillable = [
        'code', 'name', 'priority', 'flagIcon', 'viName', 'status'
    ];
    protected $guarded = [
        'code', 'name', 'priority', 'flagIcon', 'viName', 'status'
    ];

    public function getLanguages()
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('languages') && getConfig('config_debug') == 0) {
            $value = Cache::get('languages');
            return $value;
        } else {
            $query = Language::where('status', 1)->get();
            $data = [];
            foreach ($query as $item) {
                $data[$item->code] = $item;
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('languages', $data, $expiresAt);
            }
            return $data;
        }
    }
    public function saveData($request)
    {
        $code = $request->code;
        $data = \App\Models\Language::where('code', $code)->first();
        if ($data) {
            Language::where('code', $request->code)->update([
                'name' => $request->name, 'status' => $request->status, 'priority' => $request->priority, 'flagIcon' => $request->flagIcon, 'viName' => $request->viName
            ]);
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            Language::create([
                'code' => $request->code, 'name' => $request->name, 'status' => $request->status, 'priority' => $request->priority, 'flagIcon' => $request->flagIcon, 'viName' => $request->viName
            ]);
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function search($request)
    {
        $query = Language::where(function ($query) use ($request) {
            if (isset($request->name) && $request->name != '') {
                $query->where('name', 'like', '%' . $request->name . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
        })
            ->select('language.*')
            ->orderBy('priority', 'ASC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }

        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }
        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $code = $request->code;
        $data = Language::where('code', $code)->first();
        return response()->json([
            'success' => true,
            'message' => "Get success",
            'data' => $data,
        ]);
    }

    public function changeStatus($request)
    {
        Language::where('code', $request->code)->update([
            'status' => $request->status,
        ]);
        return response()->json([
            'success' => true,
            "message" => "Change status success",
        ]);
    }
}
