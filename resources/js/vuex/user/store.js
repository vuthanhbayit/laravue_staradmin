﻿import * as actions from './actions'
import CONSTANTS from '../../core/utils/constants';

let initUser = null;
if (typeof (localStorage) !== 'undefined') {
    try {
        initUser = JSON.parse(localStorage.getItem(CONSTANTS.CURRENT_USER) || null);
    } catch (e) {

    }
}

const store = {
    state: {
        token: localStorage.getItem(CONSTANTS.ACCESS_TOKEN) || null,
        permission: localStorage.getItem(CONSTANTS.ACCESS_PERMISSION) || null,
        isLoggedIn: false,
        currentUser: initUser,
        listUser: null,
    },

    mutations: {
        RETRIEVE_TOKEN(state, token) {
            state.token = token
        },
        DESTROY_TOKEN(state) {
            state.token = null
        },
        LOGGED_IN: (state, payload) => {
            state.currentUser = payload;
            state.isLoggedIn = payload != null;
        },
        LOGGED_IN_ERROR: (state, payload) => {

        },
        PERMISSION(state, permission) {
            state.permission = permission
        },
        GET_LIST_USER(state, payload) {
            state.listUser = payload;
        },
        set_rolesDetail(state, rolesDetail) {
            state.rolesDetail = rolesDetail
        },
    },
    actions,
    getters: {
        currentUser: state => state.currentUser,
        isLoggedIn: state => state.isLoggedIn,
        listUser: state => state.listUser,
    }
};

export default store;
