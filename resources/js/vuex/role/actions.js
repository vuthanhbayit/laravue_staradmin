import axios from '../../core/plugins/http';

export const getListRole = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/role/searchRole',
                ...opts
            }
        }).then(response => {
            commit('GET_LIST_ROLE', response);
            return resolve(response);
        }).catch(error => {
            return reject(error)
        })
    })
};
export const getDetailRole = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/role/detailRole',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error)
        })
    })
};
export const saveRole = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/role/saveRole',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error)
        })
    })
};
export const deleteRole = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/role/deleteRole',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error)
        })
    })
};
export const savePermissionRole = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/role/savePermissionRole',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error)
        })
    })
};
