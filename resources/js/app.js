require('./bootstrap');

import Vue from 'vue';
import store from './vuex/index';
import router from './router';

//import all component in _common
import {importAll} from "./core/utils/importComponents";
importAll(require.context('./components/_common', true, /\.vue$/));

//config element ui
import {Select, Option, Upload} from 'element-ui';
import 'element-ui/lib/theme-chalk/select.css';
import 'element-ui/lib/theme-chalk/icon.css';
import langEl from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
locale.use(langEl);
Vue.component('el-select',Select);
Vue.component('el-option',Option);
Vue.component('el-upload',Upload);

import Entry from './components/entry.vue';

var Loading = null;
import 'vue-loading-overlay/dist/vue-loading.css';
if (typeof (window) !== 'undefined') {
    Loading = require('vue-loading-overlay');
}
Vue.use(Loading);

import alert from './core/plugins/alert';
import appSettings from './core/mixins/appSettings';
import CONSTANTS from './core/utils/constants';
import metaInfo from "./core/mixins/metaInfo";
import checkPermission from './core/mixins/checkPermission';
import multiLanguage from './core/mixins/multiLanguage';
import CurrentInfo from './core/mixins/currentInfo';
Vue.mixin(metaInfo);
Vue.mixin(checkPermission);
Vue.mixin(multiLanguage);
Vue.mixin(CurrentInfo);


const app = new Vue({
    el: '#app',
    router,
    store,
    ...Entry
});
if (!process.env.VUE_ENV)
    window.app = app;
export {app, router, store};

