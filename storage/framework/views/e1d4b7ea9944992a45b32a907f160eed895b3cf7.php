<?php $__env->startSection('content'); ?>
    <?php
        echo $content_top;
    ?>
    <?php if(count($breadcrumbs) > 0): ?>
        <div class="product wp-breadcumb">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-breadcumb">
                            <?php $__currentLoopData = $breadcrumbs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><a href="<?php echo e($breadcrumb['href']); ?>"><span><?php echo e($breadcrumb['text']); ?></span></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if(!empty($data['product'])): ?>
        <div class="product package-details-sakura pt-header pd-sakura">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content-package-details">
                            <div class="panel-head-package-details d-flex">
                                <div class="address mr-auto heading-3"><?php echo e($data['product']['title']); ?></div>
                            </div>
                            <div class="panel-body-package-details">
                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-12 pr-package-details-0">
                                        <div id="product-preview" class="product-images">
                                            <?php if($data['product']['image']): ?>
                                                <div class="col-big-img" id="sys_center_img">
                                                    <div class="image col-lg-12 col-md-12">
                                                        <div class="row">
                                                            <div class="cell-center sys_show_img_big"><img
                                                                    src="<?php echo e(asset($data['product']['image'])); ?>"
                                                                    id="v7_BD_ZoomImg"
                                                                    data-zoom-image="<?php echo e(asset($data['product']['image'])); ?>"
                                                                    class="sys_img_big img-responsive"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <div class="preview-thumbs">
                                                <div class="image col-lg-12 col-md-12">
                                                    <div class="row">
                                                        <?php if($data['images']): ?>
                                                            <div id="sys_col_list_thumb" class="col-list-thumb">
                                                                <div
                                                                    class="wrap-thumb wrap-thumb sys_show_img_big sys_thumb_slide_">
                                                                    <a href="javascript:void(0);"
                                                                       data-image="<?php echo e(asset($data['product']['image'])); ?>"
                                                                       data-zoom-image="<?php echo e(asset($data['product']['image'])); ?>"
                                                                       class="v7_DB_Imgbor">
                                                                        <img
                                                                            data-img-big="<?php echo e(asset($data['product']['image'])); ?>"
                                                                            data-zoom-image="<?php echo e(asset($data['product']['image'])); ?>"
                                                                            src="<?php echo e(asset($data['product']['image'])); ?>"
                                                                            style="background-image: url('<?php echo e(asset($data['product']['image'])); ?>')"/>
                                                                    </a>
                                                                </div>
                                                                <?php $__currentLoopData = $data['images']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <div
                                                                        class="wrap-thumb wrap-thumb sys_show_img_big sys_thumb_slide_<?php echo e($i); ?>">
                                                                        <a href="javascript:void(0);"
                                                                           data-image="<?php echo e(asset($image['image'])); ?>"
                                                                           data-zoom-image="<?php echo e(asset($image['image'])); ?>"
                                                                           class="v7_DB_Imgbor">
                                                                            <img
                                                                                data-img-big="<?php echo e(asset($image['image'])); ?>"
                                                                                data-zoom-image="<?php echo e(asset($image['image'])); ?>"
                                                                                src="<?php echo e(asset($image['image'])); ?>"
                                                                                style="background-image: url('<?php echo e(asset($image['image'])); ?>')"/>
                                                                        </a>
                                                                    </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                <div id="sys_btn_slide_thumbs" class="btn-slide-thumbs">
                                                                    <i class="fa fa-angle-right"></i>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-12 pl-package-details-0">
                                        <div class="info-package-details">
                                            <ul class="list-info">
                                                <li>
                                                    <div class="wp-item-package-details heading-3">
                                                    <span class="icon">
                                                        <img src="/images/icon-package-1.png" alt="">
                                                    </span>
                                                        <span
                                                            class="text"><?php echo e(getLanguage('CodeClient')); ?>: <?php echo e($data['product']['code']); ?></span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-lg-7 col-md-6 col-sm-6 col-12">
                                                            <div class="wp-item-package-details heading-3">
                                                            <span class="icon">
                                                                <img src="/images/icon-package-2.png" alt="">
                                                            </span>
                                                                <span
                                                                    class="text"><?php echo e(getLanguage('DayNumberClient')); ?>: <?php echo e($data['product']['dayNumber']); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 col-md-6 col-sm-6 col-12">
                                                            <div class="wp-item-package-details heading-3">
                                                            <span
                                                                class="text"><?php echo e(getLanguage('DepartAddressClient')); ?>: <?php echo e($data['product']['departAddress']); ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-lg-7 col-md-6 col-sm-6 col-12">
                                                            <div class="wp-item-package-details heading-3">
                                                            <span class="icon"><img src="/images/icon-package-3.png"
                                                                                    alt=""></span>
                                                                <span
                                                                    class="text"><?php echo e(getLanguage('DepartTimeClient')); ?>: <?php echo e(date("d-m-Y H:i", strtotime($data['product']['departTime']))); ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="wp-item-package-details heading-3">
                                                    <span class="icon"><img src="/images/icon-package-4.png"
                                                                            alt=""></span>
                                                        <span
                                                            class="text"><?php echo e(getLanguage('SeatsExistClient')); ?>: <?php echo e($data['product']['seatsExist']); ?></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="program-tour-sakura pd-sakura">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content-program-tour">
                            <?php if(!empty($data['product'])): ?>
                            <div class="panel-head-program-tour panel-head-general text-center">
                                <div class="row">
                                    <div class="col-12 col-sm-10 m-auto">
                                        <h2 class="heading-2"><?php echo e(getLanguage('TourProgramClient')); ?></h2>
                                        <div class="rule"></div>
                                        <div class="intro heading-3">
                                            <?php echo $data['product']['description'] ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <?php if(count($data['day']) > 0): ?>
                                <?php
                                    $li = ''; $content = '';
                                ?>
                                <?php $__currentLoopData = $data['day']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                        if($i == 0) $active = 'active';else $active = '';
                                        $li .='<li class="tab-big '.$active.'">
                                                <a id="day-'.$i.'-tab" data-toggle="pill" href="#day-'.$i.'" role="tab"
                                                   aria-selected="true">
                                                    <div class="tab tab-1 heading-3">'.$item['title'].'</div>
                                                </a>
                                            </li>';
                                        $content .= '<div class="tab-pane fade '.$active.' show" id="day-'.$i.'">
                                            <div class="content-tab-program-tour animated fadeInUp">
                                                <div class="intro heading-3">
                                                    '.$item['content'].'
                                                </div>
                                            </div>
                                        </div>';
                                    ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <div class="row">
                                    <div class="col-12 col-sm-10 col-lg-10 col-md-10 m-auto">
                                        <div class="panel-body-program-tour">
                                            <div class="tab-program-tour">
                                                <ul class="nav nav-pills list" id="pills-tab" role="tablist">
                                                    <?php echo $li; ?>
                                                </ul>
                                            </div>

                                            <div class="tab-content" id="pills-tabContent">
                                                <?php echo $content; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if(count($data['attribute']) > 0): ?>
            <div class="detail-tour-sakura pd-sakura">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-detail-tour">
                                <div class="panel-head-detail-tour panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 col-sm-10 m-auto">
                                            <h2 class="heading-2"><?php echo e(getLanguage('DetailTourClient')); ?></h2>
                                            <div class="rule"></div>
                                        </div>
                                    </div>
                                </div>
                                <?php $__currentLoopData = $data['attribute']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="row">
                                        <div class="col-12">
                                            <h4 class="heading-4 name-info-tour mb-30">
                                                <span class="icon"><img src="<?php echo e(asset($item['icon'])); ?>" alt=""></span>
                                                <span class="text"><?php echo e($item['title']); ?></span>
                                            </h4>
                                            <div class="wp-big-info-tour mb-30 row">
                                                <div class="col-12">
                                                    <?php echo $item['content']; ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if(trim($data['product']['content']) != ''): ?>
            <div class="information-sakura pd-sakura">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-information-sakura">
                                <div class="panel-head-information panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 col-sm-10 m-auto">
                                            <h2 class="heading-2"><?php echo e(getLanguage('ContentTourClient')); ?></h2>
                                            <div class="rule"></div>
                                            <p class="detail-information-sakura heading-3">
                                                <?php echo $data['product']['content'] ; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if(count($data['related']) > 0): ?>
            <div class="travel-package pd-sakura">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-travel-package">
                                <div class="panel-head-travel-package panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 col-sm-10 m-auto">
                                            <h2 class="heading-2"><?php echo e(getLanguage('ProductRelateClient')); ?></h2>
                                            <div class="rule"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body-travel-package">
                                    <ul class="list row">
                                        <?php $__currentLoopData = $data['related']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                                <article class="article-travel-package">
                                                    <div class="img-thumb">
                                                        <a href="/p<?php echo e($item['id']); ?>" class="image-cover image">
                                                            <img src="<?php echo e(asset($item['image'])); ?>" alt=""
                                                                 class="img-fluid image-cover">
                                                        </a>
                                                    </div>
                                                    <div class="info">
                                                        <div class="meta">
                                                            <?php echo e($item['dayNumber']); ?> <?php echo e(getLanguage('StartFromClient')); ?> <?php echo e($item['price']); ?> <?php echo e(getConfig('config_currency')); ?>

                                                        </div>
                                                        <h3 class="title"><a  href="/p<?php echo e($item['id']); ?>"><?php echo e($item['title']); ?></a>
                                                        </h3>
                                                        
                                                        
                                                        
                                                    </div>
                                                </article>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php
        echo $content_bottom;
    ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <?php
        echo $footer;
    ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\xampp\htdocs\sakura.amela\resources\views/client/page/product.blade.php ENDPATH**/ ?>